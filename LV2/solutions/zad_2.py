import re

filename = 'mbox-short.txt'

try:
    fhand = open(filename)
except:
    print('File', filename, 'not found')
    exit()

users = []
for line in fhand:
    emailsInLine = re.findall(r'[\w\.\_-]+@', line)
    
    for e in emailsInLine:
        users.extend(re.findall(r'[\w\.\_-]+', e))

# print(users)


a1 = []
a2 = []
a3 = []
a4 = []
a5 = []

for u in users: 
    if re.match(r'[^a]*a[^a]*', u):
        a1.append(u)
    if re.match(r'^[^a]*a[^a]*$', u):
        a2.append(u)
    if re.match(r'[^a]', u):
        a3.append(u)
    if re.match(r'[^\d]*\d+[^\d]', u):
        a4.append(u)
    if re.match(r'^[a-z]*$', u):
        a5.append(u)

print(a1)
print(a2)
print(a3)
print(a4)
print(a5)