import re

filename = 'mbox-short.txt'

try:
    fhand = open(filename)
except:
    print('File', filename, 'not found')
    exit()

users = []
for line in fhand:
    emailsInLine = re.findall(r'[\w\.\_-]+@', line)
    
    for e in emailsInLine:
        users.extend(re.findall(r'[\w\.\_-]+', e))

print(users)