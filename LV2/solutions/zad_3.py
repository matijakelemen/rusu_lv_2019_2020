import numpy as np
import matplotlib.pyplot as plt

N = 20

a = np.random.randint(2, size = N)
v = np.ones(N)

M = np.count_nonzero(a)
Z = N - M

for i in range(N):
    if a[i] == 1:
        v[i] = np.random.normal(loc=180, scale=7, size=1)
    else:
        v[i] = np.random.normal(loc=167, scale=7, size=1)

avgm = np.dot(v, a) / M
avgz = np.dot(v, 1-a) / Z

# print(avgm)
# print(avgz)

plt.bar(v, a)
plt.show()