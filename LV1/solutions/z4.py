o = input('Unesite broj ili "Done": ')
l = []

while (o != "Done"):
    try:
        l.append(float(o))
    except ValueError:
        print("Unos ignoriran")
    finally:
        o = input('Unesite broj ili "Done": ')

print('Broj unosa', len(l))
if len(l) > 0:
    print('Srednja vrijednost', sum(l)/len(l))
    print('Minimalna vrijednost', min(l))
    print('Maksimalna vrijednost', max(l))
