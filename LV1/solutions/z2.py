o_input = input('Unesite ocjenu izmedu 0.0 i 1.0: ')

try:
    o_broj = float(o_input)
except ValueError:
    print('Broj nije float')
    exit()

if o_broj < 0.0 or o_broj > 1.0:
    print('Broj nije unutar granica [0, 1]')
    exit()

if o_broj >= 0.9:
    print('A')
elif o_broj >= 0.8:
    print('B')
elif o_broj >= 0.7:
    print('C')
elif o_broj >= 0.6:
    print('D')
else:
    print('F')