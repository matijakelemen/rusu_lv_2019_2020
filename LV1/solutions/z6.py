fname = "mbox-short.txt"

try:
    fhand = open(fname)
except:
    print('File', fname, 'not found')
    exit()

emails = []
for line in fhand:
    if line.startswith('From:'):
        emails.append(line.split()[1])

dictionary = {}
for email in emails:
    hostname = email.split('@')[1]
    if not hostname in dictionary:
        dictionary[hostname] = 1
    else:
        dictionary[hostname] += 1

print("\nSome emails:")
for email in emails[::10]:
    print(email)

print("\nDictionary entries which appear more than 4 times:")
for key, value in dictionary.items():
    if value > 4:
        print(key, value)