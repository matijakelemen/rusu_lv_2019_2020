fname = input("Unesite ime datoteke: ")

try:
    fhand = open(fname)
except:
    print('File', fname, 'not found')
    exit()

confidences = []
for line in fhand:
    if line.startswith('X-DSPAM-Confidence:'):
        c = line.split(":")[1].rstrip()
        try:
            confidences.append(float(c))
        except ValueError:
            print("Confidence not found, skipped")

print('Average X-DSPAM-Confidence: ', sum(confidences)/len(confidences))
