#%%
import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET

#%%
# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=1.1.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = list(list(root))[i]
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek
df['isWeekend'] = df['dayOfweek'] >= 6

#%%
print(df.sort_values(by='mjerenje').tail(3)['vrijeme'].dt.date)
# %%
df[['mjerenje', 'month']].groupby('month').sum().plot(kind='bar')
# %%
df[(df.month == 1) | (df.month == 8)].boxplot(column=['mjerenje'], by=['month'])
# %%
df.boxplot(column=['mjerenje'], by=['isWeekend'])
# %%
