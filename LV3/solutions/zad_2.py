# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('/home/mkelemen/GIT/rusu_lv_2019_2020/LV3/resources/mtcars.csv')

# %%
mtcars.groupby('cyl').mean()['mpg'].plot(kind='bar')
# %%
mtcars.boxplot(column=['wt'], by=['cyl'])
# %%
df = pd.DataFrame({'Mjenjac':['Auto', 'Manual'], 'mpg': [
    mtcars[mtcars.am == 1]['mpg'].mean(),
    mtcars[mtcars.am == 0]['mpg'].mean(),
]})
df.plot.bar(x='Mjenjac', y='mpg', rot=0)
# %%
ax1 = mtcars[mtcars.am == 1].plot(x='qsec', y='hp')
ax1 = mtcars[mtcars.am == 0].plot(x='qsec', y='hp')
mtcars.sort_values(by=['qsec']).groupby('am').plot(x='qsec', y='hp')
# %%
x = np.linspace(0, 2*np.pi, 400)
y = np.sin(x**2)

# Create just a figure and only one subplot
fig, ax = plt.subplots()
ax.plot(x, y)
ax.set_title('Simple plot')
# %%
