import pandas as pd

mtcars = pd.read_csv('./LV3/resources/mtcars.csv')
print(mtcars.sort_values(by=['mpg']).head(5)) #1
print(mtcars[mtcars.cyl == 8].sort_values(by=['mpg']).tail(3)) #2
print(mtcars[mtcars.cyl == 6].mpg.mean()) #3
print(mtcars[(mtcars.cyl == 6) & (mtcars.wt >= 2) & (mtcars.wt <= 2.2)].mpg.mean()) #4
print(mtcars[mtcars.am == 0].shape[0], mtcars[mtcars.am == 1].shape[0]) #5
print(mtcars[(mtcars.hp > 100) & (mtcars.am == 1)].shape[0]) #6
mtcars['kg'] = mtcars['wt'] / 2.2 * 1000 #7
print(mtcars[['car', 'kg']]) #7